/+dub.json:
{
	"name": "demo-lightning",
	"dependencies": { "sfmlish": { "path": ".." } },
}
+/
import sfmlish;
import std.stdio;
import std.algorithm;
import std.random;
import std.conv;
import std.math;
import std.range;
import std.file;
import std.path;

class TestScreen: Screen
{
	private LineShape[] segments;
	private AppWindow win;
	private Vec2[] vertices;
	private Vec2 target;
	private Rect[] wallAreas;
	private RectangleShape[] walls;
	private bool fadeBolt;
	private Clock fadeTimer;

	this() { }
	override void setWindow(AppWindow w) { win = w; }
	override void initialize() { remakeWalls(); fadeTimer = new Clock; }
	override void event(Event e)
	{
		e.handle!(
			(OnClose _) => win.close,
			(OnResize r) { win.view = new View(r.viewport); remakeWalls(); },
			(OnMousePress m) {
				vertices = [ Vec2(20, win.size.y/2) ];
				target = m.pos;
				fadeBolt = false;
			}
		);
	}

	override void update(double dt)
	{
		segments = segments.remove!((s) => s.color.a == 0);
		segments.each!((s) => s.updateProperties);
		int newBranches = to!int(70*dt*vertices.length^^0.25);
		if(uniform01() < (70*dt - newBranches)) newBranches++;
		newBranches.iota.each!((_) => addBranch());
	}

	private void remakeWalls()
	{
		wallAreas = [
			Rect(.2*win.size.x, 0, .1*win.size.x, .8*win.size.y),
			Rect(.45*win.size.x, .2*win.size.y, .1*win.size.x, .8*win.size.y),
			Rect(.7*win.size.x, 0, .1*win.size.x, .8*win.size.y),
		];
		walls = [];
		foreach(wall; wallAreas)
		{
			auto r = new RectangleShape;
			r.color = Color.Red;
			r.pos = Vec2(wall.left, wall.top);
			r.size = Vec2(wall.width, wall.height);
			walls ~= r;
		}
	}

	private void addBranch()
	{
		if(vertices.empty) return;
		Vec2 expandTowards = uniform01() < 1/sqrt(.25*vertices.length) ?
			target + 500 * uniform01()^^2 * Vec2(1,0).rotated(uniform(0.,360.)) :
			Rect(0,0,win.size.x,win.size.y).relativePoint(Vec2(uniform01(), uniform01()));
		Vec2 nearestVertex = vertices.minElement!((v) => (expandTowards - v).abs);
		Vec2 delta = expandTowards - nearestVertex;
		double segmentLength = clamp(uniform(.3,.6)*delta.abs, 0, 70);
		Vec2 newVertex = nearestVertex + delta.normalized * segmentLength;

		if(wallAreas.any!((w) => w.contains(newVertex)) || !Rect(0,0,win.size.x,win.size.y).contains(newVertex))
		{
			addBranch(); return;
		}

		LineShape segment = new LineShape(nearestVertex, newVertex, 10 + segmentLength/5.);
		segment.texture = "glow";
		segment.color = () => Color(to!ubyte(120+segmentLength), to!ubyte(120+segmentLength), 255, clamp(255-(fadeBolt ? fadeTimer.elapsed*600 : 0), 0, 255).to!ubyte);
		segments ~= segment;

		if((target - newVertex).abs <= 50.)
		{
			vertices = [];
			fadeBolt = true;
			fadeTimer.restart;
		}
		else
		{
			vertices ~= newVertex;
		}
	}

	override void updateInactive(double dt) { }
	override void draw() { segments.each!((x) => win.draw(x, RenderStates(AdditiveBlending))); walls.each!((w) => win.draw(w));}
	override void finish() { }
}

void main(string[] args)
{
	args[0].dirName.absolutePath.chdir;
	auto dir = new GameDataDirectory("lightning");
	GameData.initialize(dir);
	Config.parseMasterConfig("textures: textures.yaml");
	Textures.initialize;
	Textures.load("textures");

	auto win = new AppWindow(VideoMode.desktopMode, "Test", WindowStyle.None);
	win.pushScreen(new TestScreen());
	Mainloop.addWindow(win);
	Mainloop.run;
}
