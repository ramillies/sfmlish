/+dub.json:
{
	"name": "demo-particles",
	"dependencies": { "sfmlish": { "path": ".." } },
}
+/
import sfmlish;
import std.stdio;
import std.algorithm;
import std.random;
import std.conv;
import std.math;

class TestScreen: Screen
{
	private CircleShape[] particles;
	private AppWindow win;
	private Clock time;

	this() { }
	override void setWindow(AppWindow w) { win = w; }
	override void initialize()
	{
		time = new Clock;
	}
	override void event(Event e)
	{
		e.handle!(
			(OnClose _) => win.close,
			(OnResize r) => win.view = new View(r.viewport),
		);
	}

	override void update(double dt)
	{
		int particlesToEmit = (dt*800* exp(2*sin(time.elapsed*PI/2)^^50)).to!int;
		foreach(n; 0 .. particlesToEmit)
		{
			auto p = new CircleShape;
			p.radius = uniform(10, 30);
			p.pointCount = 100;
			p.color = ((ubyte initialRed, double emitTime) {
				Clock c = new Clock;
				double rate = (1 - sin(emitTime*PI/2)^^150)*uniform(1., 2.);
				double colorize = uniform(.1, .6);
				return () => Color(initialRed, clamp(initialRed*colorize - c.elapsed*10*rate, 0., 255.).to!ubyte, 0, clamp(15 - c.elapsed*rate, 0., 255.).to!ubyte);
			})(uniform(200, 255).to!ubyte, time.elapsed);
			p.pos = ((Vec2 start, double emitTime) {
				Clock c = new Clock;
				Vec2 velocity = Vec2(150 + 30*(1 - .6*sin(emitTime*PI/2)^^150)*uniform(-1.,1.), 0).rotated(-90+(1+sin(emitTime*sqrt(2.))/8)*uniform(-45., 45.));
				return () => start + c.elapsed * velocity;
			})(Mouse.position(win), time.elapsed);
			particles ~= p;
		}
		particles.each!((p) => p.updateProperties);
		particles = particles.remove!((p) => !(Rect(0,0,win.size.x,win.size.y).contains(p.pos)));
	}

	override void updateInactive(double dt) { }
	override void draw() { particles.each!((x) => win.draw(x, RenderStates(AdditiveBlending))); }
	override void finish() { }
}

void main()
{
	auto win = new AppWindow(VideoMode(800, 600), "Test");
	win.pushScreen(new TestScreen());
	Mainloop.addWindow(win);
	Mainloop.run;
}
