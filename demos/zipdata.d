/+dub.json:
{
	"name": "demo-zipped-data",
	"dependencies": { "sfmlish": { "path": ".." } },
}
+/
import sfmlish;
import std.stdio;
import std.algorithm;
import std.random;
import std.conv;
import std.math;
import std.range;
import std.file: thisExePath;
import std.path;

class TestScreen: Screen
{
	private AppWindow win;
	private Sprite s;

	this() { }
	override void setWindow(AppWindow w) { win = w; }
	override void initialize()
	{
		s = new Sprite;
		s.relativeOrigin = Vec2(.5, .5);
		s.scale = Vec2(4,4);
		s.pos = win.size * .5;
		s.texture = "creatures";

		Musics.get("music").play;
	}

	override void event(Event e)
	{
		e.handle!(
			(OnClose _) => win.close,
			(OnResize r) { win.view = new View(r.viewport); },
			(OnKeyPress k) {
				if(k.key == Key.Add)
					s.tilenumber = s.tilenumber+1;
				if(k.key == Key.Subtract)
					s.tilenumber = s.tilenumber-1;
			}
		);
	}

	override void update(double dt) { }
	override void updateInactive(double dt) { }
	override void draw() { win.draw(s); }
	override void finish() { }
}

void main()
{
	auto dir = new GameDataZip(thisExePath);
	GameData.initialize(dir);
	Config.parseMasterConfig("textures: textures.yaml\nmusic: music.yaml");
	Textures.initialize;
	Textures.load("textures");
	Musics.initialize;
	Musics.load("music");

	auto win = new AppWindow(VideoMode.desktopMode, "Test", WindowStyle.None);
	win.pushScreen(new TestScreen());
	Mainloop.addWindow(win);
	Mainloop.run;
}
