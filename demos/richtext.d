/+dub.json:
{
	"name": "demo-richtext",
	"dependencies": { "sfmlish": { "path": ".." } },
}
+/
import sfmlish;
import std.stdio;
import std.algorithm;
import std.random;
import std.conv;
import std.math;
import std.range;
import std.file;
import std.path;

const string[] markups = [
`Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 

{vspace 50%}{right-align}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

{vspace 50%}{center}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 

{vspace 50%}{justify}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`,

`{justify}Nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi, nej{-}ne{-}ob{-}hos{-}po{-}da{-}řo{-}va{-}tel{-}něj{-}ší{-}mi.`,

`{center}{size 200%}{underline on}Seznam monster

{left-align}{vspace 100%}{reset}Máme {color red}2×{icon(full-height,color=red) creatures/skeleton}{color white}, {color blue}3×{icon(as=Í,allow-stretching) creatures/ghost}{color white} a {color green}5×{icon(full-height,color=green) creatures/spider}{color white}.
ÁÁÁÁÁÁÁÁÁÁÁÁÁ! Ne, šéfe, to je fakt všechno!`
];

class TestScreen: Screen
{
	private AppWindow win;
	private RichText t;
	private Text info;
	private LineShape lx, ly;
	private int markupIndex;

	this() { }
	override void setWindow(AppWindow w) { win = w; }
	override void initialize()
	{
		markupIndex = 0;

		t = new RichText;
		t.font = "garamond";
		t.fontSize = 45;
		t.textBoxWidth = 1000;
		t.markup = () => markups[markupIndex];
		t.pos = Vec2(200,0);

		info = new Text;
		info.font = "italic";
		info.fontSize = 30;
		info.text = "Left/Right arrows to switch various texts. Space to show/hide bounding boxes.";
		info.relativeOrigin = Vec2(0.5, 1);
		info.pos = win.size * Vec2(0.5, 1) - Vec2(0, 5);

		lx = new LineShape;
		lx.setStartEnd(Vec2(200,0), Vec2(200,win.size.y));
		lx.color = Color.Red;

		ly = new LineShape;
		ly.setStartEnd(Vec2(1200,0), Vec2(1200,win.size.y));
		ly.color = Color.Red;
	}

	override void event(Event e)
	{
		e.handle!(
			(OnClose _) => win.close,
			(OnResize r) { win.view = new View(r.viewport); },
			(OnKeyPress k) {
				if(k.key == Key.Left)
					markupIndex = (markupIndex+2) % 3;
				if(k.key == Key.Right)
					markupIndex = (markupIndex+1) % 3;
				if(k.key == Key.Space)
					t.glyphDebugBoxes = !t.glyphDebugBoxes;
			}
		);
	}

	override void update(double dt) { t.updateProperties; }
	override void updateInactive(double dt) { }
	override void draw() { win.draw(t); win.draw(lx); win.draw(ly); win.draw(info); }
	override void finish() { }
}

void main(string[] args)
{
	args[0].dirName.absolutePath.chdir;
	auto dir = new GameDataDirectory("richtext");
	GameData.initialize(dir);
	Config.parseMasterConfig("fonts: fonts.yaml\ntextures: textures.yaml");
	Fonts.initialize;
	Fonts.load("fonts");
	Textures.initialize;
	Textures.load("textures");
	
	auto win = new AppWindow(VideoMode.desktopMode, "Test", WindowStyle.None);
	win.pushScreen(new TestScreen());
	Mainloop.addWindow(win);
	Mainloop.run;
}
