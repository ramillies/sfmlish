module sfmlish.window.clipboard;

import sfmlish.util;
import sfmlish.csfml;

class Clipboard
{
	static @property string content()
	{
		const(uint) *str = sfClipboard_getUnicodeString();
		return uintPtrToString(str);
	}

	static @property void content(string s)
	{
		uint[] codepoints = stringToUintArray(s);
		sfClipboard_setUnicodeString(codepoints.ptr);
	}
}
