module sfmlish.window.touch;

import std.conv;

import sfmlish.csfml;
import sfmlish.window.window;
import sfmlish.graphics.renderwindow;
import sfmlish.system.vec;

class Touch
{
	static bool down(uint finger) { return sfTouch_isDown(finger).to!bool; }
	static Vec2 pos(uint finger, Window relativeTo) { return Vec2.fromVector2i(sfTouch_getPosition(finger, relativeTo.internalPtr)); }
	static Vec2 pos(uint finger, WindowBase relativeTo) { return Vec2.fromVector2i(sfTouch_getPositionWindowBase(finger, relativeTo.internalPtr)); }
	static Vec2 pos(uint finger, RenderWindow relativeTo) { return Vec2.fromVector2i(sfTouch_getPositionRenderWindow(finger, relativeTo.internalPtr)); }
	static Vec2 posRelativeToDesktop(uint finger) { return Vec2.fromVector2i(sfTouch_getPosition(finger, null)); }
}
