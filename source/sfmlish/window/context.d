module sfmlish.window.context;

import sfmlish.csfml;

import sfmlish.window.contextsettings;

import std.conv;
import std.string;

class Context
{
	private sfContext *context;

	this() { context = sfContext_create(); }
	~this() { sfContext_destroy(context); }

	bool activate() { return sfContext_setActive(context, 1).to!bool; }
	bool deactivate() { return sfContext_setActive(context, 0).to!bool; }

	@property static ulong activeContextID() { return sfContext_getActiveContextId(); }
	@property ContextSettings settings() { return ContextSettings.fromSfStruct(sfContext_getSettings(context)); }

	bool hasExtension(string name) { return sfContext_isExtensionAvailable(name.toStringz).to!bool; }
	extern (C) void function() nothrow @nogc glFunction(string name) { return sfContext_getFunction(name.toStringz); }
}
