module sfmlish.callbackmixins;

struct GenerateCallback { }

enum propertyCallbacksMixin = `mixin GeneratePropertyCallbacksTemplate; mixin(_propertyCallbacksMixin());`;

mixin template GeneratePropertyCallbacksTemplate()
{
	private static string _propertyCallbacksMixin()
	{
		import std.traits: hasUDA, isCallable, Parameters, ReturnType, hasFunctionAttributes;
		import std.array: appender;
		import std.format: format;

		auto bodyParts = appender!string;
		auto updateParts = appender!string;

		alias thisClass = typeof(this);
		static foreach(member; __traits(allMembers,thisClass))
		{{
			 alias symbol = __traits(getMember, thisClass, member);
			 static if(hasUDA!(symbol, GenerateCallback) && isCallable!symbol)
			 {
				 alias params = Parameters!symbol;
				 static if(hasFunctionAttributes!(symbol, "@property"))
				 {
					static if(is(ReturnType!symbol == void) && params.length == 1)
					{
						enum propertyType = params[0].stringof;
					}
					else static if(params.length == 0)
					{
						enum propertyType = ReturnType!symbol.stringof;
					}
					enum code = format(`private %s delegate() _%s_callback;
						@property void %2$s(%1$s delegate() callback) { _%2$s_callback = callback; }
						void %2$s_cancelCallback() { _%2$s_callback = null; };`,
						propertyType, member
					);
					bodyParts.put(code);
					updateParts.put(format(`if(_%s_callback !is null) this.%1$s = _%1$s_callback();`, member));
				}
				else
				{
				       bodyParts.put(`pragma(msg, "The @GenerateCallback attribute is intended to be used on properties only!");`);
				}
			 }
		}}

		return bodyParts[] ~ `void updateProperties() { updateOrigin(); ` ~ updateParts[] ~ `}`;
	}
}
