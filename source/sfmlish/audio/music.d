module sfmlish.audio.music;

import std.conv;
import std.string;

import sfmlish.csfml;
import sfmlish.audio.soundstatus;
import sfmlish.audio.mixins;
import sfmlish.system.inputstream;
import sfmlish.system.vec;
import sfmlish.system.exception;

struct MusicABLoop
{
	float start, end;
}

class Music
{
	this(sfMusic *b) { t = b; }
	~this() { stop(); sfMusic_destroy(t); }

	static Music fromFile(string path)
	{
		auto result = sfMusic_createFromFile(path.toStringz);
		if(result is null) throw new SFMLException("Failed to load music from file '" ~ path ~ "'.");
		return new Music(result);
	}

	static Music fromMemory(const(ubyte)[] bytes)
	{
		auto result = sfMusic_createFromMemory(bytes.ptr, bytes.length);
		if(result is null) throw new SFMLException("Failed to load music from memory.");
		return new Music(result);
	}

	static Music fromStream(InputStream stream)
	{
		sfInputStream s = stream.toSfInputStream;
		auto result = sfMusic_createFromStream(&s);
		if(result is null) throw new SFMLException("Failed to load music from input stream " ~ stream.to!string ~ ".");
		return new Music(result); 
	}

	mixin(soundMixinForType("sfMusic")); 

	@property float duration() const { return sfTime_asSeconds(sfMusic_getDuration(t)); }
	@property uint channels() const { return sfMusic_getChannelCount(t); }
	@property uint samplerate() const { return sfMusic_getSampleRate(t); }

	@property MusicABLoop ABLoop() const
	{
		sfTimeSpan span = sfMusic_getLoopPoints(t);
		return MusicABLoop(sfTime_asSeconds(span.offset), sfTime_asSeconds(span.offset) + sfTime_asSeconds(span.length));
	}

	@property void ABLoop(MusicABLoop loop)
	{
		sfMusic_setLoopPoints(t, sfTimeSpan(sfSeconds(loop.start), sfSeconds(loop.end - loop.start)));
	}
}
