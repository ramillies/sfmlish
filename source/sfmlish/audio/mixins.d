module sfmlish.audio.mixins;

import std.format;

string soundMixinForType(string type)
{
	return format(`
	private %1$s* t;

	void play() { %1$s_play(t); }
	void pause() { %1$s_pause(t); }
	void stop() { %1$s_stop(t); }

	@property bool loop() const { return %1$s_getLoop(t).to!bool; }
	@property void loop(bool b) { %1$s_setLoop(t, b.to!uint); }

	@property SoundStatus status() const { return cast(SoundStatus) %1$s_getStatus(t); }

	@property float speed() const { return %1$s_getPitch(t); }
	@property float volume() const { return %1$s_getVolume(t); }
	@property Vec3 spatialPosition() const { return Vec3.fromVector3f(%1$s_getPosition(t)); }
	@property bool relativeToListener() const { return %1$s_isRelativeToListener(t).to!bool; }
	@property float pos() const { return sfTime_asSeconds(%1$s_getPlayingOffset(t)); }
	@property float minDistance() const { return %1$s_getMinDistance(t); }
	@property float attenuation() const { return %1$s_getAttenuation(t); }

	@property void speed(float f) { %1$s_setPitch(t, f); }
	@property void volume(float f) { return %1$s_setVolume(t, f); }
	@property void spatialPosition(Vec3 pos) { %1$s_setPosition(t, pos.toVector3f); }
	@property void relativeToListener(bool b) { %1$s_setRelativeToListener(t, b.to!uint); }
	@property void pos(float f) { %1$s_setPlayingOffset(t, sfSeconds(f)); }
	@property void minDistance(float f) { %1$s_setMinDistance(t, f); }
	@property void attenuation(float f) { %1$s_setAttenuation(t, f); }

	void disableSpatialization()
	{
		this.relativeToListener = true;
		this.spatialPosition = Vec3(0,0,0);
	}

	
	`, type);
}
