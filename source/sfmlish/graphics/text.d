module sfmlish.graphics.text;

import sfmlish.csfml;

import sfmlish.system.rect;
import sfmlish.system.vec;
import sfmlish.graphics.transformable;
import sfmlish.graphics.transform;
import sfmlish.graphics.color;
import sfmlish.graphics.font;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.drawable;
import sfmlish.graphics.renderstates;
import sfmlish.util;
import sfmlish.callbackmixins;
import sfmlish.game.resources;

import std.typecons: Rebindable;

enum TextStyle : uint { Regular = 0U, Bold = 1U, Italic = 2U, Underlined = 4U, Strikethrough = 8U }

class Text: Transformable, Drawable, Bboxable
{
	private sfText *t;
	private Rebindable!(const(Font)) currentFont;

	this() { t = sfText_create(); }
	this(sfText *ptr) { t = ptr; }
	~this() { sfText_destroy(t); }

	@GenerateCallback
	{
		@property Color color() const { return Color.fromSfColor(sfText_getFillColor(t)); }
		@property void color(Color c) { sfText_setFillColor(t, c.toSfColor()); }
		@property Color outlineColor() const { return Color.fromSfColor(sfText_getOutlineColor(t)); }
		@property void outlineColor(Color c) { sfText_setOutlineColor(t, c.toSfColor()); }
	}

	mixin(transformationsForType("sfText"));
	mixin(relativeOriginMixin);
	mixin(bboxesForType("sfText"));
	
	@property const(Font) font() const { return currentFont; }
	@property void font(const(Font) f) { sfText_setFont(t, (cast(Font) f).internalPtr); currentFont = f; }

	@GenerateCallback
	{
		@property uint fontSize() const { return sfText_getCharacterSize(t); }
		@property void fontSize(uint size) { sfText_setCharacterSize(t, size); }
		@property float lineSpacing() const { return sfText_getLineSpacing(t); }
		@property void lineSpacing(float f) { sfText_setLineSpacing(t, f); }
		@property float letterSpacing() const { return sfText_getLetterSpacing(t); }
		@property void letterSpacing(float f) { sfText_setLetterSpacing(t, f); }
		@property uint style() const { return sfText_getStyle(t); }
		@property void style(uint f) { sfText_setStyle(t, f); }

		@property float outlineThickness() const { return sfText_getOutlineThickness(t); }
		@property void outlineThickness(float f) { sfText_setOutlineThickness(t, f); }
				
		@property string text() const { return uintPtrToString(sfText_getUnicodeString(t)); }
		@property void text(string s) { sfText_setUnicodeString(t, stringToUintArray(s).ptr); }
	}

	@property sfText *internalPtr() { return t; }

	Vec2 characterPos(size_t n) const { return Vec2.fromVector2f(sfText_findCharacterPos(t, n)); }

	mixin(drawableMixin);
	mixin(propertyCallbacksMixin);

	@property void font(string name) { font = Fonts.get(name); }
}
