module sfmlish.graphics.vertexbuffer;

import std.conv;
import std.array;
import std.algorithm;

import sfmlish.csfml;
import sfmlish.graphics.vertex;
import sfmlish.graphics.primitivetype;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.graphics.drawable;
import sfmlish.system.rect;

enum VertexBufferUsage { Stream, Dynamic, Static }

class VertexBuffer: Drawable
{
	private sfVertexBuffer *t;

	static bool isSupported() { return sfVertexBuffer_isAvailable.to!bool; }

	this(uint vertices, PrimitiveType type, VertexBufferUsage usage) { t = sfVertexBuffer_create(vertices, cast(sfPrimitiveType) type, cast(sfVertexBufferUsage) usage); }
	this(sfVertexBuffer *v) { t = v; }
	~this() { sfVertexBuffer_destroy(t); }

	VertexBuffer dup() const { return new VertexBuffer(sfVertexBuffer_copy(t)); }
	
	@property PrimitiveType primitiveType() const { return cast(PrimitiveType) sfVertexBuffer_getPrimitiveType(t); }
	@property void primitiveType(PrimitiveType p) { sfVertexBuffer_setPrimitiveType(t, cast(sfPrimitiveType) p); }
	@property VertexBufferUsage usage() const { return cast(VertexBufferUsage) sfVertexBuffer_getUsage(t); }
	@property void usage(VertexBufferUsage u) { sfVertexBuffer_setUsage(t, cast(sfVertexBufferUsage) u); }

	@property size_t vertexCount() const { return sfVertexBuffer_getVertexCount(t); }

	bool update(Vertex[] vertices, uint offset = 0)
	{
		sfVertex[] v = vertices.map!((v) => v.toSfVertex()).array;				
		return sfVertexBuffer_update(t, v.ptr, v.length.to!uint, offset).to!bool;
	}

	bool update(VertexBuffer other)
	{
		return sfVertexBuffer_updateFromVertexBuffer(t, other.internalPtr).to!bool;
	}

	mixin(drawableMixin);

	@property sfVertexBuffer *internalPtr() { return t; }
	@property uint nativeHandle() { return sfVertexBuffer_getNativeHandle(t); }
}
