module sfmlish.graphics.image;

import sfmlish.csfml;
import sfmlish.graphics.color;
import sfmlish.system.rect;
import sfmlish.system.vec;
import sfmlish.system.inputstream;
import sfmlish.system.exception;

import std.array;
import std.string;
import std.conv;

class Image
{
	private sfImage *t;

	this(sfImage *ptr) { t = ptr; }
	~this() { sfImage_destroy(t); }

	static Image create(uint width, uint height)
	{
		auto result = sfImage_create(width, height);
		if(result is null) throw new SFMLException("Failed to create empty image of dimensions " ~ width.to!string ~ " × " ~ height.to!string ~ ".");
		return new Image(result);
	}

	static Image fromColor(uint width, uint height, Color color)
	{
		auto result = sfImage_createFromColor(width, height, color.toSfColor);
		if(result is null) throw new SFMLException("Failed to create image of dimensions " ~ width.to!string ~ " × " ~ height.to!string ~ " filled with " ~ color.to!string ~ ".");
		return new Image(result);
	}

	static Image fromPixels(uint width, uint height, const(ubyte)[] pixels)
	{
		auto result = sfImage_createFromPixels(width, height, pixels.ptr);
		if(result is null) throw new SFMLException("Failed to load image from pixels.");
		return new Image(result);
	}

	static Image fromFile(string path)
	{
		auto result = sfImage_createFromFile(path.toStringz);
		if(result is null) throw new SFMLException("Failed to load image from file '" ~ path ~ "'.");
		return new Image(result);
	}

	static Image fromMemory(const(ubyte)[] bytes)
	{
		auto result = sfImage_createFromMemory(cast(const(void) *) bytes.ptr, bytes.length);
		if(result is null) throw new SFMLException("Failed to load image from memory.");
		return new Image(result);
	}

	static Image fromStream(InputStream stream)
	{
		sfInputStream s = stream.toSfInputStream;
		auto result = sfImage_createFromStream(&s);
		if(result is null) throw new SFMLException("Failed to load image from input stream " ~ stream.to!string ~ ".");
		return new Image(result);
	}


	Image dup() const { return new Image(sfImage_copy(t)); }

	bool saveToFile(string path) { return sfImage_saveToFile(t, path.toStringz).to!bool; }
	bool saveToMemory(ref ubyte[] bytes, string format)
	{
		sfBuffer *buf = sfBuffer_create();
		bool retval = sfImage_saveToMemory(t, buf, format.toStringz).to!bool;
		if(retval)
			bytes = sfBuffer_getData(buf)[0 .. sfBuffer_getSize(buf)].dup;
		sfBuffer_destroy(buf);
		return retval;
	}

	@property Vec2 size() const { return Vec2.fromVector2u(sfImage_getSize(t)); }
	void transparentColor(Color c, ubyte alpha = 0) { sfImage_createMaskFromColor(t, c.toSfColor, alpha); }
	void copy(Image source, Vec2 dest, Rect sourceRect = Rect(0,0,0,0), bool applyAlpha = true) { sfImage_copyImage(t, source.t, dest.x.to!uint, dest.y.to!uint, sourceRect.toIntRect, applyAlpha.to!uint); }

	void setPixel(Vec2 pos, Color c) { sfImage_setPixel(t, pos.x.to!uint, pos.y.to!uint, c.toSfColor); }
	Color getPixel(Vec2 pos) const { return Color.fromSfColor(sfImage_getPixel(t, pos.x.to!uint, pos.y.to!uint)); }

	void horizontalFlip() { sfImage_flipHorizontally(t); }
	void verticalFlip() { sfImage_flipVertically(t); }

	sfImage *internalPtr() { return t; }
}
