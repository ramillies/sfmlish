module sfmlish.graphics.glyph;

import sfmlish.csfml;
import sfmlish.system.rect;

struct Glyph
{
	float advance;
	Rect bounds, textureRect;

	sfGlyph toSfStruct() const { return sfGlyph(advance, bounds.toFloatRect, textureRect.toIntRect); }
	static Glyph fromSfStruct(sfGlyph g) { return Glyph(g.advance, Rect.fromFloatRect(g.bounds), Rect.fromIntRect(g.textureRect)); }
}
