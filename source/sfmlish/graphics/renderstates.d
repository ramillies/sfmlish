module sfmlish.graphics.renderstates;

import sfmlish.csfml;
import sfmlish.graphics.texture;
import sfmlish.graphics.blendmode;
import sfmlish.graphics.transform;
import sfmlish.graphics.shader;

import std.typecons: Rebindable;

struct RenderStates
{
	BlendMode blendMode = AlphaBlending;
	Matrix transform = IdentityMatrix;
	Rebindable!(const(Texture)) texture = null;
	Rebindable!(const(Shader)) shader = null;

	sfRenderStates toSfRenderStates()
	{
		return sfRenderStates(
			blendMode.toSfStruct,
			transform.internal,
			texture is null ? null : (cast(Texture) texture).internalPtr,
			shader is null ? null : (cast(Shader) shader).internalPtr
		);
	}

	static RenderStates fromSfRenderStates(sfRenderStates r)
	{
		auto rv = RenderStates(
			BlendMode.fromSfStruct(r.blendMode),
			Matrix(r.transform)
		);
		rv.texture = cast(const(Texture)) new Texture(sfTexture_copy(r.texture));
		rv.shader = cast(const(Shader)) new Shader(cast(sfShader *) r.shader);
		return rv;
	}
}
