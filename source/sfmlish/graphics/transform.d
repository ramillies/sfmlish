module sfmlish.graphics.transform;

import sfmlish.graphics.transformable;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.callbackmixins;

import sfmlish.csfml;

class Transform: Transformable
{
	private sfTransformable* t;

	this() { t = sfTransformable_create(); }
	
	mixin(transformationsForType("sfTransformable"));
}

struct Matrix
{
	private sfTransform t = sfTransform_Identity;
	
	static Matrix fromElements(float a, float b, float c, float d, float e, float f, float g, float h, float i) { return Matrix(sfTransform_fromMatrix(a,b,c,d,e,f,g,h,i)); }

	@property float[3][3] elements() const { return [ [ t.matrix[0], t.matrix[1], t.matrix[2] ], [ t.matrix[3], t.matrix[4], t.matrix[5] ], [ t.matrix[6], t.matrix[7], t.matrix[8] ] ]; }
	@property void elements(float[3][3] e) { t = sfTransform_fromMatrix(e[0][0], e[0][1], e[0][2], e[1][0], e[1][1], e[1][2], e[2][0], e[2][1], e[2][2]); }
	
	float[16] gl4x4Matrix() const { float[16] rv; sfTransform_getMatrix(&t, rv.ptr); return rv; }

	Matrix inverse() const { return Matrix(sfTransform_getInverse(&t)); }

	Matrix translate(Vec2 d) const { sfTransform m = t; sfTransform_translate(&m, d.x, d.y); return Matrix(m); }
	Matrix rotate(float degrees, Vec2 center = Vec2(0,0)) const { sfTransform m = t; sfTransform_rotateWithCenter(&m, degrees, center.x, center.y); return Matrix(m); }
	Matrix scale(Vec2 d, Vec2 center = Vec2(0,0)) const { sfTransform m = t; sfTransform_scaleWithCenter(&m, d.x, d.y, center.x, center.y); return Matrix(m); }

	Vec2 opBinary(string op)(Vec2 rhs) const
	if(op == "*")
	{
		return Vec2.fromVector2f(sfTransform_transformPoint(&t, rhs.toVector2f));
	}

	Rect opBinary(string op)(Rect rhs) const
	if(op == "*")
	{
		return Rect.fromFloatRect(sfTransform_transformRect(&t, rhs.toFloatRect));
	}

	Matrix opBinary(string op)(Matrix rhs) const
	if(op == "*")
	{
		sfTransform_combine(&rhs.t, &t);
		return Matrix(rhs.t);
	}

	sfTransform internal() const { return t; }
}

enum IdentityMatrix = Matrix();
