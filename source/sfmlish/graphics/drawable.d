module sfmlish.graphics.drawable;

import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;

import sfmlish.csfml;

enum drawableMixin = `override void draw(RenderTarget target, RenderStates states)
{
	target.draw(this, states);
}`;

interface Drawable
{
	void draw(RenderTarget target, RenderStates states);
}
