module sfmlish.graphics.blendmode;

import sfmlish.csfml;

enum BlendFactor {
	Zero,
	One,
	SrcColor,
	OneMinusSrcColor,
	DstColor,
	OneMinusDstColor,
	SrcAlpha,
	OneMinusSrcAlpha,
	DstAlpha,
	OneMinusDstAlpha,
}

enum BlendEquation {
	Add,
	Subtract,
	ReverseSubtract,
	Min,
	Max,
}

struct BlendMode {
	BlendFactor colorSrcFactor;
	BlendFactor colorDstFactor;
	BlendEquation colorEquation;
	BlendFactor alphaSrcFactor;
	BlendFactor alphaDstFactor;
	BlendEquation alphaEquation;

	sfBlendMode toSfStruct() const
	{
		return sfBlendMode(
			cast(sfBlendFactor) colorSrcFactor,
			cast(sfBlendFactor) colorDstFactor,
			cast(sfBlendEquation) colorEquation,
			cast(sfBlendFactor) alphaSrcFactor,
			cast(sfBlendFactor) alphaDstFactor,
			cast(sfBlendEquation) alphaEquation
		);
	}

	static fromSfStruct(sfBlendMode s)
	{
		return BlendMode(
			cast(BlendFactor) s.colorSrcFactor,
			cast(BlendFactor) s.colorDstFactor,
			cast(BlendEquation) s.colorEquation,
			cast(BlendFactor) s.alphaSrcFactor,
			cast(BlendFactor) s.alphaDstFactor,
			cast(BlendEquation) s.alphaEquation
		);
	}
}

enum AlphaBlending = BlendMode.fromSfStruct(sfBlendAlpha);
enum AdditiveBlending = BlendMode.fromSfStruct(sfBlendAdd);
enum MultiplicativeBlending = BlendMode.fromSfStruct(sfBlendMultiply);
enum NoBlending = BlendMode.fromSfStruct(sfBlendNone);
enum MinBlending = BlendMode.fromSfStruct(sfBlendMin);
enum MaxBlending = BlendMode.fromSfStruct(sfBlendMax);
