module sfmlish.graphics.lineshape;

import sfmlish.graphics.convexshape;
import sfmlish.graphics.transform;
import sfmlish.graphics.transformable;
import sfmlish.graphics.drawable;
import sfmlish.graphics.color;
import sfmlish.graphics.renderstates;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.vertexarray;
import sfmlish.graphics.vertex;
import sfmlish.graphics.primitivetype;
import sfmlish.graphics.texture;
import sfmlish.graphics.textureaccessmixin;
import sfmlish.callbackmixins;
import sfmlish.system.rect;
import sfmlish.system.vec;

import std.math;
import std.algorithm;
import std.range;
import std.stdio;
import std.typecons;

class LineShape: Transformable, Drawable, Bboxable
{
	private
	{
		Vec2 end = Vec2(0,0);
		double _thickness = 0.;
		Color _color;
		VertexArray vertices;
		Rebindable!(const Texture) tex;
		Rect _textureRect;
	}

	this() { vertices = new VertexArray(); transform = new Transform; updateVertices; }
	this(Vec2 a, Vec2 b, double t) { end = b-a; _thickness = t; vertices = new VertexArray(); transform = new Transform; pos = a; updateVertices; }

	mixin(customTransformable);
	mixin(transformableBbox);
	mixin(relativeOriginMixin);

	bool isThin() const { return _thickness <= 0.; }
	Vec2 cornerPoint(int n) const
	{
		int k = n%4;
		Vec2 halfEndSide = end.abs == 0. ? Vec2(0,0) : end.normalized.perp * (max(_thickness, 0.)/2.);
		return
			k == 0 ? halfEndSide :
			k == 1 ? -halfEndSide :
			k == 2 ? end - halfEndSide :
			end + halfEndSide;
	}

	@property Rect localBounds() const
	{
		return Rect(minElement(4.iota.map!((n) => cornerPoint(n).x)), minElement(4.iota.map!((n) => cornerPoint(n).y)),
			abs(maxElement(4.iota.map!((n) => cornerPoint(n).x)) - minElement(4.iota.map!((n) => cornerPoint(n).x))),
			abs(maxElement(4.iota.map!((n) => cornerPoint(n).y)) - minElement(4.iota.map!((n) => cornerPoint(n).y)))
		);
	}

	private void updateVertices()
	{
		vertices.clear;
		if(isThin())
		{
			auto v = [
				Vertex(Vec2(0,0), _color),
				Vertex(end, _color),
			];
			vertices.append(v);
			vertices.primitiveType = PrimitiveType.Lines;
		}
		else
		{
			if(tex is null)
			{
				vertices.append([
					Vertex(cornerPoint(0), _color),
					Vertex(cornerPoint(1), _color),
					Vertex(cornerPoint(2), _color),
					Vertex(cornerPoint(3), _color)
				]);
			}
			else
			{
				vertices.append([
					Vertex(cornerPoint(0), _color, _textureRect.relativePoint(Vec2(0, 0))),
					Vertex(cornerPoint(1), _color, _textureRect.relativePoint(Vec2(1, 0))),
					Vertex(cornerPoint(2), _color, _textureRect.relativePoint(Vec2(1, 1))),
					Vertex(cornerPoint(3), _color, _textureRect.relativePoint(Vec2(0, 1)))
				]);
			}
			vertices.primitiveType = PrimitiveType.Quads;
		}
	}

	@GenerateCallback
	{
		@property Vec2 span() const { return end; }
		@property void span(Vec2 v) { end = v; updateVertices; }
		@property Color color() const { return _color; }
		@property void color(Color c) { _color = c; updateVertices; }
		@property double thickness() const { return _thickness; }
		@property void thickness(double d) { _thickness = d; updateVertices; }
		@property const(Texture) texture() const { return tex; }
		@property void texture(const Texture t)
		{
			tex = t;
			useTextureFromStore = false;
			_textureRect = Rect(0, 0, t.size.x, t.size.y);
			updateVertices;
		}
		@property Rect textureRect() const { return _textureRect; }
		@property void textureRect(Rect r) { _textureRect = r; updateVertices; }
	}

	void unsetTexture() { tex = null; useTextureFromStore = false; }

	void setStartEnd(Vec2 s, Vec2 e)
	{
		scale = Vec2(1,1);
		rotation = 0.;
		pos = s;
		end = e - s;
		updateVertices;
	}

	override void draw(RenderTarget target, RenderStates states)
	{
		states.transform = transform.matrix * states.transform;
		states.texture = cast(Texture) tex;
		target.draw(vertices, states);
	}

	mixin(propertyCallbacksMixin);
	mixin(textureAccessMixin);
}
