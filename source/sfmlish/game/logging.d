module sfmlish.game.logging;

import std.logger;
import std.stdio;

private Logger _sfmlishLogger;

@property public Logger sfmlishLogger()
{
	if(_sfmlishLogger is null)
		_sfmlishLogger = new StdForwardLogger;
	return _sfmlishLogger;
}

@property public void sfmlishLogger(Logger l) { _sfmlishLogger = l; }
