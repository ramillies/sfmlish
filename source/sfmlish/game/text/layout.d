module sfmlish.game.text.layout;

import sfmlish.graphics.glyph;
import sfmlish.graphics.font;
import sfmlish.graphics.vertexarray;
import sfmlish.graphics.vertex;
import sfmlish.graphics.primitivetype;
import sfmlish.graphics.color;
import sfmlish.graphics.texture;
import sfmlish.graphics.transform;
import sfmlish.graphics.transformable;
import sfmlish.graphics.rendertarget;
import sfmlish.graphics.renderstates;
import sfmlish.graphics.drawable;
import sfmlish.callbackmixins;
import sfmlish.system.vec;
import sfmlish.system.rect;
import sfmlish.game.resources;
import sfmlish.game.text.markup;
import sfmlish.game.text.renderer;

import std.typecons: Rebindable;
import std.algorithm;
import std.range;
import std.stdio;
import std.string;
import std.sumtype;

struct MarkupLayout
{
	float boxSize = 0.;

	private
	{
		float lineWidth = 0, y = 0;
		MarkupElement[] line;
		Appender!(Symbol[]) _symbols;
		Appender!(DrawLine[]) _lines;
		Alignment alignment = Alignment.Left;
	}

	private DrawLine makeLineStruct(float leftX, float rightX, float height, float thickness, float outlineThickness, Color color)
	{
		leftX = horizontalPosWithAlign(leftX);
		rightX = horizontalPosWithAlign(rightX);
		return DrawLine(
			Rect(
				leftX - outlineThickness,
				y + height - thickness/2 - outlineThickness,
				rightX - leftX + 2*outlineThickness,
				thickness + 2*outlineThickness
			),
			color
		);
	}

	private void addDrawLine(T)(T lineStart, float currentX)
	{
		with(lineStart)
		if(on)
		{
			if(outlineThickness > 0)
				_lines ~= makeLineStruct(xPos, currentX, height, thickness, outlineThickness, outlineColor);
			_lines ~= makeLineStruct(xPos, currentX, height, thickness, 0, fillColor);
		}
	}

	private float horizontalPosWithAlign(float x)
	{
		if(alignment == Alignment.Right)
			return x + boxSize - lineWidth;
		else if(alignment == Alignment.Center)
			return x + (boxSize - lineWidth)/2;
		else return x;
	}

	private void typesetLine(MarkupElement[] instructions, bool breakDueToOverfill)
	{
		auto lastUnderline = Underline(false);
		auto lastStrikethrough = Strikethrough(false);
		y += instructions.map!((e) => e.match!((Symbol s) => s.lineHeight, (_) => 0)).maxElement;
		float x = 0;
		auto result = appender!(Symbol[]);

		float justifyExpandFactor = 0.;
		if(alignment == Alignment.Justify && breakDueToOverfill)
		{
			float justifySpaceSum = instructions.map!((e) => e.match!((HSpace s) => s.expandable ? s.advance : 0, (_) => 0.)).sum;
			justifyExpandFactor = justifySpaceSum < 1. ? 0. : (boxSize - lineWidth) / justifySpaceSum;
		}

		foreach(e; instructions)
		{
			e.match!(
				(VSpace v) { y += v.height; },
				(Symbol s)
				{
					s.pos = Vec2(horizontalPosWithAlign(x), y - s.raiseHeight) - s.outlineThickness * Vec2(1,1);
					result ~= s;
				},
				(Underline u)
				{
					addDrawLine(lastUnderline, x);
					u.xPos = x;
					lastUnderline = u;
				},
				(Strikethrough s)
				{
					addDrawLine(lastStrikethrough, x);
					s.xPos = x;
					lastStrikethrough = s;
				},
				(TabJump j) { x = j.x; },
				(HSpace h) { x += h.expandable ? h.advance * justifyExpandFactor : 0; },
				(_) { }
			);
			x += e.advance;
		}
		_symbols ~= result[];
		addDrawLine(lastUnderline, x);
		addDrawLine(lastStrikethrough, x);
	}

	void layOutElements(MarkupElement[] elements)
	{
		auto lastUnderline = MarkupElement(Underline(false));
		auto lastStrikethrough = MarkupElement(Strikethrough(false));
		_symbols = appender!(Symbol[]);
		_lines = appender!(DrawLine[]);
		float lastBreakpointX = boxSize;
		foreach(e; elements)
		{
			lineWidth += e.advance;
			if(e.tabJump > 0)
				lineWidth = e.tabJump;
			if(e.isUnderline)
				lastUnderline = e;
			if(e.isStrikethrough)
				lastStrikethrough = e;
			if(e.isBreakpoint)
			{
				lastBreakpointX = lineWidth;
			}
			e.match!((Align a) { alignment = a.alignment; }, (_) { });
			if(boxSize > 0 && lineWidth > boxSize)
			{
				lineWidth = lastBreakpointX;
				auto lastBreakpoint = line.retro.countUntil!((x) => x.isBreakpoint);
				if(lastBreakpoint == -1)
				{
					typesetLine(line, true);
					line = [ ];
				}
				else
				{
					Symbol[] addSymbols = line[$-lastBreakpoint-1].match!((Breakpoint b) => b.symbolsAtBreak, (_) => cast(Symbol[]) []);
					lineWidth += addSymbols.map!((s) => s.advance).sum;
					typesetLine(line[0 .. $ - lastBreakpoint - 1] ~ addSymbols.map!((s) => MarkupElement(s)).array, true);
					line = line[$-lastBreakpoint .. $];
				}
				line.skipOver!((e) => e.isWhitespace);
				line = [ lastUnderline, lastStrikethrough ] ~ line;
				lineWidth = line.map!((e) => e.advance).sum;
				lastBreakpointX = boxSize;

				if(e.isWhitespace && !line.any!((e) => e.isSymbol)) continue;
				else lineWidth += e.advance;
			}
			if(e == MarkupElement(Newline()))
			{
				typesetLine(line, false);
				line = [ lastUnderline, lastStrikethrough ];
				lineWidth = e.advance;
				lastBreakpointX = boxSize;
			}
			line ~= e;
		}
		typesetLine(line, false);
	}

	@property Symbol[] symbols() { return _symbols[]; }
	@property DrawLine[] lines() { return _lines[]; }
}
